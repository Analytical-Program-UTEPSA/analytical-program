package com.analyticalprogram;

import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class AnalyticalApplication extends Application<AnalyticalConfiguration> {


    private final HibernateBundle<AnalyticalConfiguration> hibernateBundle = new
        HibernateBundle<AnalyticalConfiguration>(AnalyticalApplication.class) {

        @Override
        public DataSourceFactory getDataSourceFactory(AnalyticalConfiguration analyticalConfiguration) {
                return analyticalConfiguration.getDatabase();
            }
        };

    public static void main(final String[] args) throws Exception {
        new AnalyticalApplication().run(args);
    }

    @Override
    public String getName() {
        return "Analytical";
    }

    @Override
    public void initialize(final Bootstrap<AnalyticalConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final AnalyticalConfiguration configuration,
                    final Environment environment) {
        // TODO: implement application
    }

}
